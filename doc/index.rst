p_astro
=======

`p_astro` Low-latency estimation of category-wise astrophysical
probability of GW candidates.

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    introduction
    em_bright
    p_astro_module

.. toctree::
    :maxdepth: 1

    Git repository <https://git.ligo.org/lscsoft/p-astro>
